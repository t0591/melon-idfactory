# melon-idfactory
主键工厂，提供ID生成服务，保证ID的唯一性。
<br><br>
使用motan rpc + restful接口两种调用方式，简单配置，快速部署，使用方便。

### 目前提供3中ID服务：
1. 提供唯一有序的，不重复的64位整数id生成服务（推荐使用）
2. 提供自增长整数ID生产服务
3. 提供32位UUID生产服务


## 使用说明

### 1.依赖安装
项目运行依赖JDK1.8和MongoDB，安装方法自行百度
* JDK[传送门](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* MongoDB[传送门](https://www.mongodb.com/)<br>

### 2.发布版下载
获取最新版本的服务包，目前版本为v1.0.0，[下载地址](https://gitee.com/fetech-platform/melon-idfactory/releases/v1.0.0-Release)<br>
<br>
解压melon-idfactory-server-assembly.tar.gz，目录如下：
* bin 运行脚本，提供windows和linux两种运行方式
* conf 配置文件
* lib 项目库

### 3.配置说明
melon-idfactory的配置很简单,而且都提供默认配置，一般使用时关注少量配置即可
>\# conf.properties
>>\# mongodb连接配置<br>
>>mongo.host=127.0.0.1<br>
>>mongo.port=27017&nbsp;&nbsp;&nbsp;#默认为"27017"<br> 
>>mongo.databaseName=MelonIdFactory&nbsp;&nbsp;&nbsp;#默认MelonMongoDbDefault<br>
>
>>\# uid-generator配置，与64位有序ID的功能有关，可以不做改动，全部使用默配置<br>
>>\#[uid-generator配置详细介绍](https://github.com/baidu/uid-generator/blob/master/README.zh_cn.md#%E6%AD%A5%E9%AA%A43-%E4%BF%AE%E6%94%B9spring%E9%85%8D%E7%BD%AE)<br>
>>uid.boostPower=3<br>
>>uid.paddingFactor=50<br>
>>uid.scheduleInterval=60<br>
>>uid.timeBits=28<br>
>>uid.workerBits=22<br>
>>uid.seqBits=13<br>
>>uid.epochStr=2017-10-1<br>
>
>>\# RPC远程调用配置,使用motan提供rpc服务和restful接口<br>
>>motan.service.export=8002&nbsp;&nbsp;&nbsp;# Java服务暴露端口，默认为"8002"<br>
>>motan.restful.export=8004&nbsp;&nbsp;&nbsp;&nbsp;# Restful接口暴露端口，默认为"8004"<br>
>><br>
>>\# Java服务支持使用zookeeper或consul为注册中心，配置如下，默认为direct直连，可以不配置<br>
>>motan.registry.regProtocol=zookeeper<br>
>>motan.registry.address=127.0.0.1:2181<br>

### 4.运行
进入server的bin目录，执行启动脚本
* start.bat
* start.sh
* stop.sh

（windows运行start.bat，linux运行start.sh）

PS:<br>melon-idfactory-client.jar 为测试调用包，提供了motan rpc的调用封装，使用spring的小伙伴可以引用，
<br>其他小伙伴可以直接运行server，调用restful接口即可。

## 调用说明

### restful调用说明
* 测试服务是否正常<br>
GET http://localhost:8004/restful/isConnected 
* 获取32位UUID<br>
GET http://127.0.0.1:8004/idfactory/getUUID
* 获取自增的整数ID，key一般可以使用表名<br>
GET http://127.0.0.1:8004/idfactory/getIncrementId/{key}
* 初始化自增的整数ID，key一般可以使用表名，start表示初始值，如果是0，则从1开始<br>
POST http://127.0.0.1:8004/idfactory/initIncrementId/{key}/{start}
* 获取64位有序的唯一ID<br>
GET http://127.0.0.1:8004/idfactory/get64Uid
* 解析64位ID<br>
GET http://127.0.0.1:8004/idfactory/parse64Uid/{uid}

### client调用说明

引用melon-idfactory-client.jar<br>

可自行编译源码或者直接下载JAR包，[下载地址](https://gitee.com/fetech-platform/melon-idfactory/releases/v1.0.0-Release)<br>

目前只支持spring项目，可扩展，在你项目的spring配置文件中加入如下配置：
```xml
<context:component-scan base-package="com.fetech"/>

<bean id="propertyConfigurer" class="com.fetech.melon.context.property.MelonPropertyPlaceholderConfigurer">
    <property name="locations">
        <list>
            <value>classpath*:META-INF/melon/idfactory_conf.properties</value>
        </list>
    </property>
</bean>
```
>\# conf.properties<br>
><br>
>\# RPC服务地址配置，无注册中心时使用<br>
>motan.referer.directUrl=127.0.0.1:8002<br>
>\# 如服务使用注册中心发布，需要配置对应的注册中心地址<br>
>motan.registry.regProtocol=zookeeper<br>
>motan.registry.address=127.0.0.1:2181<br>

#### 代码中直接注入IdFactoryClient即可
```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:META-INF/spring/spring-test-idfactory-client.xml"})
public class TestIF {

    @Resource
    private IdFactoryClient idFactoryClient;

    @Test
    public void uuid() {
        LogUtil.debug("uuid:");
        for (int i = 0; i < 10; i++) {
            LogUtil.debug(idFactoryClient.getUUID());
        }
    }

    @Test
    public void uid() {
        LogUtil.debug("64 uid:");
        for (int i = 0; i < 10; i++) {
            long id = idFactoryClient.get64Uid();
            LogUtil.debug(id + "");
            LogUtil.debug(idFactoryClient.parse64Uid(id));
        }
    }

    @Test
    public void incrementId() {
        LogUtil.debug("auto increment id:");
        for (int i = 0; i < 10; i++) {
            LogUtil.debug(idFactoryClient.getIncrementId("test_1") + "");
        }
    }

    @Test
    public void incrementId2() {
        LogUtil.debug("auto increment2 id:");
        boolean ret = idFactoryClient.initIncrementId("test_2", 10);
        Assert.assertTrue(ret);
        for (int i = 0; i < 10; i++) {
            LogUtil.debug(idFactoryClient.getIncrementId("test_2") + "");
        }
    }
}
```

#### IdFactoryClient接口说明

```java
/**
 * 获取自增长的主键
 *
 * @param key 主键的key，一般可以使用表名
 * @return long exp:1,2,3...
 */
long getIncrementId(String key);

/**
 * 手动初始化自增长的主键，一般初始化一次即可，如没有初始化，默认getIncrementId从1开始
 *
 * @param key   主键的key，一般可以使用表名
 * @param start 组件的初始值，默认为0，则从1开始
 * @return boolean 初始化成功或失败
 */
boolean initIncrementId(String key, long start);

/**
 * 获取32位的UUID
 *
 * @return String exp:ea5846a348764fc4a7311d6a340e9d14,ae2653087ec84dd59b3adcf4c307cf97...
 */
String getUUID();

/**
 * 获取有序的64位的ID，推荐使用
 *
 * @return long exp:69728553533104128,69728553533104129,69728553533104130...
 */
long get64Uid();

/**
 * 解析有序的64位的ID
 *
 * @param uid 有序的64位的ID
 * @return json exp:
 * {
 * "UID": "69728553533104129",
 * "timestamp": "2017-11-08 11:42:48",
 * "workerId": "87",
 * "sequence": "1"
 * }
 */
String parse64Uid(long uid);
```

## 其他说明
1. 64位有序ID使用了百度uid-generator，是对twitter的snowflake算法的实现
* [uid-generator](https://github.com/baidu/uid-generator)
* [snowflake](https://github.com/twitter/snowflake)
2. 为什么要使用64d位的有序ID
* [架构师之路](https://mp.weixin.qq.com/s?__biz=MjM5ODYxMDA5OQ==&mid=2651960245&idx=1&sn=5cef3d8ca6a3e6e94f61e0edaf985d11&chksm=bd2d06698a5a8f7fc89056af619b9b7e79b158bceb91bdeb776475bc686721e36fb925904a67&mpshare=1&scene=23&srcid=0627tlxFe54jtvb2Ju57eIc2#rd)
* [全局唯一ID设计](http://www.jianshu.com/p/61817cf48cc3)


## 特别说明，使用melon（甜瓜）开发框架
甜瓜系列不是发明创造，只是想把事情变得简单点，给使用者一点甜头。
### [melon传送门](https://gitee.com/fetech-framework/melon)

// TODO
目前自增长整数ID不支持分布式部署，后续将改进。