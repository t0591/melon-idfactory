package com.fetech.melon.idfactory.worker.entity;

import java.util.Date;

import com.fetech.melon.dao.an.Column;
import com.fetech.melon.dao.an.ID;
import com.fetech.melon.dao.mongodb.BaseMongoEntity;
import com.fetech.melon.idfactory.worker.WorkerNodeType;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Entity for M_WORKER_NODE
 */
@SuppressWarnings("unused")
public class WorkerNodeEntity extends BaseMongoEntity {

    private static final long serialVersionUID = -8086541425017280916L;
    /**
     * Entity unique id (table unique)
     */
    @ID("ID")
    private Long id;

    /**
     * Type of CONTAINER: HostName, ACTUAL : IP.
     */
    @Column("HOST_NAME")
    private String hostName;

    /**
     * Type of CONTAINER: Port, ACTUAL : Timestamp + Random(0-10000)
     */
    @Column("PORT")
    private String port;

    /**
     * type of {@link WorkerNodeType}
     */
    @Column("TYPE")
    private int type;

    /**
     * Worker launch date, default now
     */
    @Column("LAUNCH_DATE")
    private Date launchDate = new Date();

    /**
     * Created time
     */
    @Column("CREATED")
    private Date created;

    /**
     * Last modified
     */
    @Column("MODIFIED")
    private Date modified;

    /**
     * Getters & Setters
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getLaunchDate() {
        return null == launchDate ? null : (Date) launchDate.clone();
    }

    public void setLaunchDateDate(Date launchDate) {
        this.launchDate = null == launchDate ? null : (Date) launchDate.clone();
    }

    public Date getCreated() {
        return null == created ? null : (Date) created.clone();
    }

    public void setCreated(Date created) {
        this.created = null == created ? null : (Date) created.clone();
    }

    public Date getModified() {
        return null == modified ? null : (Date) modified.clone();
    }

    public void setModified(Date modified) {
        this.modified = null == modified ? null : (Date) modified.clone();
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
