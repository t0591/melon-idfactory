package com.fetech.melon.idfactory;

import com.fetech.melon.context.log.LogUtil;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by ZhangGang on 2017/10/10.
 */
public class Start {

    public static void main(String[] args) throws InterruptedException {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:META-INF/spring/spring-melon-idfactory.xml");
        if (applicationContext.isRunning()) {
            LogUtil.info("server start...");
        }
    }

}
