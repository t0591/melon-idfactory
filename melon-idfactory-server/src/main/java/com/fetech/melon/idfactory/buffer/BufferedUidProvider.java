package com.fetech.melon.idfactory.buffer;

import java.util.List;

@FunctionalInterface
public interface BufferedUidProvider {

    /**
     * Provides UID in one second
     *
     * @param momentInSecond momentInSecond
     * @return List<Long>
     */
    List<Long> provide(long momentInSecond);
}
