package com.fetech.melon.idfactory.utils;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Represents a padded {@link AtomicLong} to prevent the FalseSharing problem<p>
 * <p>
 * The CPU cache line commonly be 64 bytes, here is a sample of cache line after padding:<br>
 * 64 bytes = 8 bytes (object reference) + 6 * 8 bytes (padded long) + 8 bytes (a long value)
 */
@SuppressWarnings("unused")
public class PaddedAtomicLong extends AtomicLong {
    private static final long serialVersionUID = -3415778863941386253L;

    /**
     * Padded 6 long (48 bytes)
     */
    private volatile long p1;
    private volatile long p2;
    private volatile long p3;
    private volatile long p4;
    private volatile long p5;

    public void setP2(long p2) {
        this.p2 = p2;
    }

    public void setP3(long p3) {
        this.p3 = p3;
    }

    public void setP4(long p4) {
        this.p4 = p4;
    }

    public void setP5(long p5) {
        this.p5 = p5;
    }

    /**
     * Constructors from {@link AtomicLong}
     */
    public PaddedAtomicLong() {
        super();
    }

    public PaddedAtomicLong(long initialValue) {
        super(initialValue);
    }

    /**
     * To prevent GC optimizations for cleaning unused padded references
     */
    public long sumPaddingToPreventOptimization() {
        long p6 = 7L;
        return p1 + p2 + p3 + p4 + p5 + p6;
    }

    public void setP1(long p1) {
        this.p1 = p1;
    }
}