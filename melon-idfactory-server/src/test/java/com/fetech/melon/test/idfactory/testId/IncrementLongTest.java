package com.fetech.melon.test.idfactory.testId;

import com.fetech.melon.context.log.LogUtil;
import com.fetech.melon.idfactory.increment.IncrementLongService;
import com.fetech.melon.test.idfactory.common.IdFactoryJunit;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by ZhangGang on 2017/10/20.
 */
public class IncrementLongTest extends IdFactoryJunit {

    @Resource
    private IncrementLongService incrementLongService;

    @Test
    public void test1() throws InterruptedException {
        //incrementLongService.init("test2", 0);
        List<Thread> threadList = new ArrayList<>();
        Set<Long> ids = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(() -> {
                for (int j = 0; j < 50; j++) {
                    Long id = incrementLongService.getNewId("test3");
                    LogUtil.debug(id + "");
                    ids.add(id);
                }
            });
            threadList.add(thread);
        }
        for (Thread aThreadList : threadList) {
            aThreadList.start();
        }
        for (Thread aThreadList : threadList) {
            aThreadList.join();
        }
        LogUtil.debug(incrementLongService.getKeys() + "");
        Assert.assertEquals(ids.size(), 5000);

    }

}
