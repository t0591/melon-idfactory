package com.fetech.melon.test.idfactory.generator;

import com.fetech.melon.context.log.LogUtil;
import com.fetech.melon.idfactory.UidGenerator;
import com.fetech.melon.idfactory.impl.CachedUidGenerator;
import com.fetech.melon.test.idfactory.common.GeneratorTest;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Test for {@link CachedUidGenerator}
 *
 * @author yutianbao
 */
public class CachedUidGeneratorTest extends GeneratorTest {

    private static final int SIZE = 100000; // 10w

    @Resource(name = "cachedUidGenerator")
    private UidGenerator uidGenerator;

    /**
     * Test for serially generate
     */
    @Test
    public void testSerialGenerate() throws IOException {
        // Generate UID serially
        Set<Long> uidSet = new HashSet<>(SIZE);
        for (int i = 0; i < SIZE; i++) {
            doGenerate(uidSet, i);
        }

        // Check UIDs are all unique
        checkUniqueID(uidSet);
    }

    /**
     * Woker run
     */
    public void workerRun(Set<Long> uidSet, AtomicInteger control) {
        for (; ; ) {
            int myPosition = control.updateAndGet(old -> (old == SIZE ? SIZE : old + 1));
            if (myPosition == SIZE) {
                return;
            }

            doGenerate(uidSet, myPosition);
        }
    }

    @Override
    public int getSize() {
        return SIZE;
    }

    /**
     * Do generating
     */
    private void doGenerate(Set<Long> uidSet, int index) {
        long uid = uidGenerator.getUID();
        String parsedInfo = uidGenerator.parseUID(uid);
        boolean existed = !uidSet.add(uid);
        if (existed) {
            LogUtil.debug("Found duplicate UID " + uid);
        }

        // Check UID is positive, and can be parsed
        Assert.assertTrue(uid > 0L);
        Assert.assertTrue(StringUtils.isNotBlank(parsedInfo));

        if (VERBOSE) {
            LogUtil.debug(Thread.currentThread().getName() + " No." + index + " >>> " + parsedInfo);
        }
    }

    /**
     * Check UIDs are all unique
     */
    public void checkUniqueID(Set<Long> uidSet) throws IOException {
        LogUtil.debug(uidSet.size()+"");
        Assert.assertEquals(SIZE, uidSet.size());
    }

}
