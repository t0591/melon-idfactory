package com.fetech.melon.idfacotry.client.api;

public interface IdFactoryApi {

    /**
     * 获取自增长的主键
     *
     * @param key 主键的key，一般可以使用表名
     * @return long exp:1,2,3...
     */
    long getIncrementId(String key);

    /**
     * 手动初始化自增长的主键，一般初始化一次即可，如没有初始化，默认getIncrementId从1开始
     *
     * @param key   主键的key，一般可以使用表名
     * @param start 组件的初始值，默认为0，则从1开始
     * @return boolean 初始化成功或失败
     */
    boolean initIncrementId(String key, long start);

    /**
     * 获取32位的UUID
     *
     * @return String exp:ea5846a348764fc4a7311d6a340e9d14,ae2653087ec84dd59b3adcf4c307cf97...
     */
    String getUUID();

    /**
     * 获取有序的64位的ID，推荐使用
     *
     * @return long exp:69728553533104128,69728553533104129,69728553533104130...
     */
    long get64Uid();

    /**
     * 解析有序的64位的ID
     *
     * @param uid 有序的64位的ID
     * @return json exp:
     * {
     * "UID": "69728553533104129",
     * "timestamp": "2017-11-08 11:42:48",
     * "workerId": "87",
     * "sequence": "1"
     * }
     */
    String parse64Uid(long uid);

}